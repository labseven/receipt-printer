# Receipt Printer Hacks

Some experiments with receipt printers.

## directory

### email

A program that prints emails (for mailing lists, or for printing attached images)

##### 🕸️TODO🕸️

- [ ] clean up the code


### loops

What happens when you loop the printed receipt back into the printer? Magic✨

### double

Experiments where one printer feeds into a second one


## global todo

- [ ] add images/videos of experiments
