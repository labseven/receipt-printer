#! /usr/bin/python3

import argparse
import textwrap

parser = argparse.ArgumentParser('Print text on receipt printer')

parser.add_argument('--port', default='/dev/usb/lp1',
                    help='printer port (/dev/usb/lp1)')
parser.add_argument('-w', '--width', type=int, default=50,
                    help="paper width in chars")
parser.add_argument('filename')

args = parser.parse_args()


text_w = args.width - 2


def format_text(text):
    return textwrap.wrap(text, text_w)


with open(args.filename) as text_file:
    print_text = []
    for line in text_file:
        print_text.extend(format_text(line))
        print_text.append("\n")

with open(args.port, 'w') as printer:
    for line in print_text:
        printer.write('  ' + line + '\n')
    for _ in range(10):
        printer.write('\n')
