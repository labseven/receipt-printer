#!/usr/bin/python3

# printer is 48 lines wide

import random

def make_line(l=48, b=" ", c="x"):
    i = random.randint(0, l-1)
    return b*i + c + b*(l-i-1)

with open('/dev/usb/lp0', 'w') as printer:
    printer.write("\n")

    # for _ in range(100):
    while True:
        if random.random() > .9:
            printer.write(make_line(c="o") + "\n")
        else:
            printer.write("\n")
