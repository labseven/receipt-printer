# Email printer
It prints emails

### Install

```
chmod +x email_idle.py
./email_idle.py
```

Use systemd for autostart/restart:
```
sudo cp email_printer.service /etc/systemd/system/
systemctl daemon-reload
systemctl start email_printer # starts the service
systemctl enable email_printer # enables autostart on boot
```
