#!/usr/bin/python3

# Print emails on a receipt printer. Lots of messy code ( as always :) ), but works well.
# you can use a basic systemd service for auto-start/restart: email_printer.service


import imapclient
import email
import time
import textwrap
import subprocess
import os
import tempfile
import re
from PIL import Image


import email_config as config

print_to = "/dev/usb/lp0" # none, adafruit, serial
printing = True


def wrapText(text, lineWidth=48):
    return '\n'.join([textwrap.fill(line, lineWidth) for line in text.splitlines() if line != "[FROM AN EXTERNAL SENDER]"])

def formatEmail(email_message, bodyText):
    date = " ".join(email_message["Date"].split(" ")[:5])
    subject = email_message["subject"]
    sender = email_message["From"].split('<')[0]

    text = ""
    text += "Subject: " + subject + "\n"
    text += "From: " + sender + "\n"
    text += date + '\n\n'
    text += bodyText[:min(len(bodyText), 1500)]

    return text

def printText(text):
    print("printing...")

    pre_pad = 4 if len(text.split('\n')) < 5 else 1
    post_pad = 12 if len(text.split('\n')) < 12 else 4

    text = wrapText(text)

    print(text)
    try:
        with open(print_to, 'w') as printer:
            printer.write('\n'*pre_pad)
            printer.write(text)
            printer.write('\n'*post_pad)
    except FileNotFoundError:
        print("can't connect to printer")

def printAttachment(email_message):
    for part in email_message.walk():
        print(part.get_content_maintype())
        if part.get_content_maintype() == 'multipart':
            continue
        if part.get('Content-Disposition') is None:
            continue
        filename = part.get_filename()
        extension = filename.split('.')[-1]
        print('file:', filename)
        if(extension == "txt"):
            typed_text = part.get_payload(decode=True).decode()
            print(typed_text)
            printText(typed_text)
        if(extension in ["jpg", "png", "jpeg", "gif"]):
            att_path = tempfile.NamedTemporaryFile().name
            with open(att_path, 'wb') as fp:
                fp.write(part.get_payload(decode=True))

            im = Image.open(att_path)

            w,h = im.size

            subprocess.run(["lpr", "-o orientation-requested=" + ("3" if h/(1.0*w) > 1.5 else "4"), "-o fit-to-page -o media=Custom.72x1000mm", "-P", "mibble", att_path])


def get_body(email_message):
    for part in email_message.walk():
        # print(part.get_content_maintype())
        if part.get_content_maintype() == 'text':
            return part.get_payload(decode=True).decode()


def printBody(email_message):
    for part in email_message.walk():
        # print(part.get_content_maintype())
        if part.get_content_maintype() == 'text':
            # print(part.get_body(preferencelist=('plain')))
            printText(formatEmail(email_message, part.get_payload(decode=True).decode()))
            break
    else:
        printText(formatEmail(email_message, ""))


def printMSCHF(email_message):
    body = get_body(email_message)
    message = re.findall(r"(?<=\r\n<https://voice.google.com>\r\n)(.*)(?=\r\nYOUR ACCOUNT <https://voice.google.com> HELP CENTER)", body)[0]
    text = "*"*48
    text += "\n"
    text += "NEW TEXT FROM MSCHF!!!\n\n"
    text += message
    text += "\n"
    text += "*"*48
    printText(text)



def printUnread():
    # Need to pause IDLE before doing imap.search
    imap.idle_done()
    # Search for unread emails, then iterate through them
    UIDs = imap.search(['UNSEEN'])
    # Mark the emails unread (so that we don't keep printing them)
    imap.remove_flags(UIDs, ["UNSEEN"])

    # Fetch each email and print it.
    for uid in UIDs:
        raw_mail = imap.fetch(uid, ["BODY[]"])
        for key in raw_mail.keys():
            if b'BODY[]' in raw_mail[key].keys():
                email_message = email.message_from_string(raw_mail[key][b'BODY[]'].decode())

                if email_message['from'] == '"Astrohaus" <hello@astrohaus.com>':
                    printAttachment(email_message)

                elif "New text message from (917) 970-7424" in email_message['subject']:
                    printMSCHF(email_message)

                elif "New text message" in email_message['subject'] or "Print this!" in email_message['subject']:
                    printAttachment(email_message)

                else:
                    printBody(email_message)


        # Pause between printing consecutive emails
        time.sleep(1)

    # Restart IDLE, because we're done fetching emails
    imap.idle()


try:
    # IMAP setup
    imap = imapclient.IMAPClient(config.imap_host, ssl=True)
    imap.login(config.imap_user, config.imap_password)
except:
    print("can't connect to server")
    exit()

# print(imap.list_folders())
imap.select_folder('INBOX')

# Print ip address / wifi status
# wifi_status = ""
# wifi_status += "Wifi Status:\n"
# wifi_status += subprocess.run("ip addr show wlan0".split(' '), capture_output=True).stdout.decode()
# printText(wifi_status)
# time.sleep(3)

# Start IDLE mode
start_time = time.monotonic()
imap.idle()
print("imap idling")

# Print emails in queue
printUnread()

while True:
    try:
        # Wait for an IDLE response
        print("listening...")
        responses = imap.idle_check(timeout=120)
        print("Server sent:", responses if responses else "nothing")

        # Need to reset the idle connection every 13 minutes
        if time.monotonic() - start_time > 13*60:
            imap.idle_done()
            imap.idle()
            start_time = time.monotonic()
        
        if responses:
            printUnread()

        
    except KeyboardInterrupt:
        break


# Clean up the IMAP connection and exit
imap.idle_done()
print("\nIDLE mode done")
imap.logout()
